# Skyrimified

Originally based off Lexy's Legacy of the Dragonborn modlist, this list aims to expand on it while cranking up the visuals to eleven and containing popular community requested mods fitting my vision of Skyrim.
 
![status](https://img.shields.io/endpoint?url=https://build.wabbajack.org/lists/status/skyrimified/badge.json?style=for-the-badge&label=Modlist%20Status&labelColor=6934e5&color=6934e5&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ4IDc5LjE2NDAzNiwgMjAxOS8wOC8xMy0wMTowNjo1NyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIxLjAgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjIxMzM0RjM2NDVDRTExRUJCNDkxOTRGNDNGM0QwMjI2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjIxMzM0RjM3NDVDRTExRUJCNDkxOTRGNDNGM0QwMjI2Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MjEzMzRGMzQ0NUNFMTFFQkI0OTE5NEY0M0YzRDAyMjYiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MjEzMzRGMzU0NUNFMTFFQkI0OTE5NEY0M0YzRDAyMjYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5oUUzBAAADuUlEQVR42uyaXUgUURTHZ7N68CEMCykwa6kEocK+WOjTHiLMsKcMekmKHiKoIAoqwoeCwod6yYqUlYyMkqKHiiAlFSrJorIy6MsitLIPSx+ywul/3SMtpjvH3Xt37uzeA7+HmT07c/9n7tx77rnjs23bSmYbZSW5mQCYAJgAmACYAJgAJLGN9nDbx4EVIA/MBWOBD6QDPzgNtjhdxOeBTHAGSAWP6HgbWAkWg/HD/OcPyAavEyEAaaAUtINcsGYYv2egCTSAGtDDubjOASiip38cfAO/wBj67SXIouN35Ht30P8ngp+g26uD4G+wA3wFzfREH4BZ4AkQT66LAjEgXowBy8AlUEXHkU30AM35bv+zfFAQdlwCskE1+BR2vgPM51zfC2OAGPxmgz56FdIHnh31jAV0fAfUgRugkXtx3QOwAZwdrvNSF/8BAqA1mhvoHID1oJrhlxOteJ0HwRKm+GOxiNd1ECy1eXZBxv10ewU2UwrrZBfBOhk31CkAIq+vZfhVguJEWw0uZIovkylelx6wClxjZG33KFAJVQ8QK7rrDPH7VYh3uwfMpBVcioPfWnAl0SpCGeA2Q/xhleLd6gEZ9D5nOvixKjpeK4ktB+cpCJHsBNhKI/5UBe0QtYKW/vvEMcMrZmZ4p8i/xFZr+f29XzPxFeR/QKHwzyBzoG3xEF/EbFiQ/PcpFP8FZIW3T7X41cyGVZH/HoXiu4B/cBtVii9kNuwc+e9SKF6U1aYP1U5V4rnduIb8yxWK76G6YdyWw4fAXoZfPU2L00AF6KANDZkmxB2JVDSRHYDtVKVxsk5KhHopG+1zKx+XmQoXMsW3UxGzl45dEy+zBwSoLO1k763QRmanLlUYGQGYDN5Yod1Zp24v6vsfdKrBxfoKpNGTdxL/0QptYGglPtYAiEXKfTDFwe8qmAPe6lh/jzYAYsdGbFD6HfwOggLqAXpaFElOGTMB2emBjdcR1wPEWr6I4Sc+aDhqecBGMguUg00MP1HkzLc8YtwxYDdTfJOXxHN7gPgi4zHjWg+t0Dc8E8CkOGpIoVykNiy7lFoT5FRlW0j8UnDL4nyaIteC4KaKWaCBMdo/Jd9FtjtWGcssEOnHOsbNn5NvwCXxJ2OdBoc6KcaFRsbNX5D/PJfEl8vIAwafSAXNjJu/Iv9cl8QHZSVC4Qc5VDJ2sjbyz3NJ/BmZmWD4NFhPo3g3lZL+mzJBG1hCU10rTUE9cRztL4ONXt8bTJjlsAmACYAJgAmACYAJgAmAt+2vAAMA3+XUYljFuWwAAAAASUVORK5CYII=)

~~[Gameplay Overview](GameplayGuide.md)~~ Working on an update!

![Skyrimified Header Image](Pictures/1.0.0-gitlab.gif)

<!-- vim-markdown-toc GitLab -->

* [Minimum PC Specifications](#minimum-pc-specifications)
* [Recommended PC Specifications](#recommended-pc-specifications)
* [Size](#size)
* [Installation](#installation)
	* [Step 1 - Preparing the Wabbajack installation environment](#step-1-preparing-the-wabbajack-installation-environment)
	* [Step 2 - Downloading Wabbajack](#step-2-downloading-wabbajack)
	* [Step 3 - Downloading the modlist](#step-3-downloading-the-modlist)
	* [Step 4 - Selecting the list through Wabbajack](#step-4-selecting-the-list-through-wabbajack)
	* [Step 5 - Selecting the installation folder](#step-5-selecting-the-installation-folder)
	* [Step 6 - Installing the modlist](#step-6-installing-the-modlist)
	* [Step 7 - Optimizing Skyrim on NVIDIA GPUs](#step-7-optimizing-skyrim-on-nvidia-gpus)
	* [Step 8 - Moving Game Folder Files](#step-8-moving-game-folder-files)
	* [Step 9 - Installing ENB](#step-9-installing-enb)
	* [Step 10 - Starting Skyrim](#step-10-starting-skyrim)
	* [MCM Settings](#mcm-settings)
		* [A Matter of Time](#a-matter-of-time)
		* [EasyWheel](#easywheel)
		* [Follower Framework](#follower-framework)
		* [iWant RND Widgets](#iwant-rnd-widgets)
		* [Keep it Clean](#keep-it-clean)
		* [moreHUD](#morehud)
		* [Not So Fast MG](#not-so-fast-mg)
		* [Not So Fast MQ](#not-so-fast-mq)
		* [Realistic Needs](#realistic-needs)
		* [SkyUI](#skyui)
		* [The Ultimate Dodge Mod](#the-ultimate-dodge-mod)
		* [Timing is Everything](#timing-is-everything)
		* [VioLens](#violens)
		* [Widget Addon](#widget-addon)
	* [](#)
	* [Frequently Asked Questions](#frequently-asked-questions)
		* [I have a 21:9 monitor. Does this modlist work with it?](#i-have-a-219-monitor-does-this-modlist-work-with-it)
		* [Is there a way to easily make the game more family friendly regarding nudity?](#is-there-a-way-to-easily-make-the-game-more-family-friendly-regarding-nudity)
		* [Can my PC run this modlist?](#can-my-pc-run-this-modlist)
		* [I want to add this mod. Can you help me with it?](#i-want-to-add-this-mod-can-you-help-me-with-it)
		* [I get an error when launching Mod Organizer mentioning a missing VCRUNTIME140_1.DLL. How do I fix that?](#i-get-an-error-when-launching-mod-organizer-mentioning-a-missing-vcruntime140_1dll-how-do-i-fix-that)
		* [When I launch the game it's stuck at a compiling shaders message on the top left. How do I fix that?](#when-i-launch-the-game-its-stuck-at-a-compiling-shaders-message-on-the-top-left-how-do-i-fix-that)
		* [Is there a way to disable the character switching in dialogue?](#is-there-a-way-to-disable-the-character-switching-in-dialogue)
		* [Can I run the modlist in different languages?](#can-i-run-the-modlist-in-different-languages)
		* [I'm getting screen tearing. What's the best way to go about fixing that?](#im-getting-screen-tearing-whats-the-best-way-to-go-about-fixing-that)
		* [Which settings can I adjust to improve graphical performance?](#which-settings-can-i-adjust-to-improve-graphical-performance)
		* [Can I re-enable pausing in menus?](#can-i-re-enable-pausing-in-menus)
		* [I'm getting NVAPI_ACCESS_DENIED when trying to apply the NVIDIA settings, how do I fix this?](#im-getting-nvapi_access_denied-when-trying-to-apply-the-nvidia-settings-how-do-i-fix-this)
		* [My player character is moving a lot in the charactar creation menu! How do I stop him/her from moving so much?](#my-player-character-is-moving-a-lot-in-the-charactar-creation-menu-how-do-i-stop-himher-from-moving-so-much)
		* [It's too dark, I can't see anything!](#its-too-dark-i-cant-see-anything)
	* [Credits](#credits)

<!-- vim-markdown-toc -->

## Minimum PC Specifications

Estimate for 1080p @ 60fps.

**Processor** Intel i7-4790K or AMD Ryzen 5 1600

**Graphics** GTX 1080 / RX 5600XT (8GB of VRAM is HIGHLY recommended for a stutter-free experience)

**Memory** 16GB - DDR4 recommended (users below 16GB may experience low framerates, stuttering and possibly even crashing)

**Storage** Ideally you'll want the modlist (downloads can be elsewhere) installed on an SSD for much faster loading times and less stuttering.

## Recommended PC Specifications

Estimate for 1440p @ 60fps.

**Processor** Intel i5-10400 or AMD Ryzen 5 3600

**Graphics** NVIDIA RTX 3070 / AMD RX 6800

**Memory** 32GB DDR4, ideally CL18 timings or below

**Storage** NVMe M.2 SSD with high read speeds

## Size

**Storage**
- Modlist Size: 222GB
- Downloads Size: 123GB
- Total Size: **345GB**

Hope that didn't scare you off - it's quite a few gigabytes. You can keep the downloads on another hard drive while installing, and you could also delete the downloads after you're done installing - be aware though that future updates will take a long time as ALL of the archives would need to be redownloaded!

# Installation

## Step 1 - Preparing the Wabbajack installation environment

First off - make sure your game is completely vanilla, you can use my [Skyrim Shredder](https://www.nexusmods.com/skyrimspecialedition/mods/30133) if you want a quick fix for that. Also make sure to launch the game at least once, and install it outside of your Program Files directories (not in the default Steam location). If you already have a Steam Library on a disk and want to put your game on there without putting it into Program Files, check out [this tool by LostDragonist.](https://github.com/LostDragonist/steam-library-setup-tool/releases) **Make sure to turn off any Creation Club content - CC mods are not supported.**

Create a Wabbajack folder and an installation folder, preferably on the root of your drive (Windows causes permission issues sometimes).

Example:

`C:\Wabbajack` (Wabbajack folder)	
`C:\Skyrimified` (Installation folder, where Mod Organizer will be installed)

**Do NOT set the installation folder to your Skyrim game directory!**

## Step 2 - Downloading Wabbajack

Get [the latest Wabbajack build](https://www.github.com/wabbajack-tools/wabbajack/releases/latest) from GitHub (the executable file named `Wabbajack.exe`).

Place it in the Wabbajack folder.

## Step 3 - Downloading the modlist

Start Wabbajack and select the modlist from the gallery, press the big download button.

This'll download the modlist file which is basically a set of instructions Wabbajack will use to replicate my installation on your computer.

## Step 4 - Selecting the list through Wabbajack

Open up `Wabbajack.exe` from your Wabbajack folder. Wabbajack will download the latest version if it's out of date.

Select *Browse Modlists*, find Skyrimified and click the download button. You can search for it at the top if you can't find it.

## Step 5 - Selecting the installation folder

Select your installation folder in Wabbajack and point it to the folder created in step 1.	

Downloads will be automatically stored in a subdirectory (`C:\Skyrimified\downloads`), but if you would like to save space on the installation drive, create an empty Downloads folder on the drive you wish to keep them on and select that instead.

## Step 6 - Installing the modlist

Click the play button in Wabbajack to start installing the modlist!

If you get an error with a specific mod while installing, that mod likely updated and removed its old files so the modlist installer needs an update.

Feel free to notify me about this in the [Wabbajack Discord](https://discord.gg/ZHXVmTs) (#skyrimified-support channel), if you haven't seen anyone else mention it yet and it isn't pinned.

If you don't like Discord, you can always reach me on here (GitLab), [GitHub](https://github.com/tr4wzified) or even on [the Nexus](https://www.nexusmods.com/users/70262113).

## Step 7 - Optimizing Skyrim on NVIDIA GPUs

If you have an NVIDIA GPU I recommend performing the following steps. Skip this if you're using an AMD card.

* Open `nvidiaProfileInspector.exe` which can be found in `<your installation folder>\Tools\NVIDIA Profile Inspector\`.
* At the top, click the little arrow pointing down to a disk.
* You should get a little dropdown, one of them saying `Import profile(s)`. Click it.
![NVIDIA Profile Inspector Icon](Pictures/README/ImportingNVIDIAProfile.png)
* Select `Skyrimified.nip` and open it. It's in the same folder you launched the Profile Inspector from.
* You should see 'Profile succesfully imported!'. Click OK and after that hit Apply at the top right.
* Close NVIDIA Profile Inspector.

If you run into the `DRS_SaveSettings failed: NVAPI_ACCESS_DENIED` error, follow the steps [here](#im-getting-an-error-applying-nvidia-profile-settings).

## Step 8 - Moving Game Folder Files

Once your installation is complete, proceed to go to your Skyrim game directory.

Copy all the files inside the Game Folder Files directory (in the installation directory) into your Skyrim game directory.

**WARNING: Only copy the files inside the Game Folder Files directory into your Skyrim folder, not all of the other files! When confused, ask me first and I'll be happy to help. Doing this incorrectly might require a reinstallation of the list, which will take some time!**

## Step 9 - Installing ENB

This modlist is designed to use with an ENB. I personally use Dragens' Ruvaak Dahmaan, I think it's the best one out there out of the ones I've tested.	
To install ENB, first head over to [the ENBSeries website](http://enbdev.com/download_mod_tesskyrimse.htm) and at the bottom, select the latest ENB version (v0.448 as of writing this).

On the next page, click the download button all the way at the bottom.	
Now open up the downloaded zip archive, go to WrapperVersion and drag **ONLY** `d3d11.dll` and `d3dcompiler_46e.dll` into your Skyrim game folder.

If you don't know where that is, right-click The Elder Scrolls V: Skyrim Special Edition in your Steam Library, select properties, go to the 'Local Files' tab and click on 'Browse Local Files'.

Now go to the [Ruvaak Dahmaan ENB Nexus page](https://www.nexusmods.com/skyrimspecialedition/mods/6009?tab=files) and download the file called 'Ruvaak Dahmaan ENB Update 10.0.1 WIP'.
Open it up with a program like [7Zip](https://www.7-zip.org/), extract the contents of the folder called 'RDENB 10 WIP' to your Skyrim game folder (so that the enbseries files are next to SkyrimSE.exe).

Next up, I recommend using my `enblocal.ini` and `enbseries.ini` instead of the one provided by the ENB. It makes the following adjustments:
- Removing the FPS limit (EnableFPSLimit=false)
- Enabling High Resolution ENB GUI scaling (HighResolutionScaling=true)
- Dropping the Ambient Occlusion resolution scaling to 0.4 (was 0.5) for higher framerates with negligible visual differences
- Disabling Depth of Field
- Disabling Lens Flares
- Enabling LE-like subsurface scattering, which I slightly prefer

[You can download those here.](https://mega.nz/file/Pdl0DTxa#A2hInCx_8gPTib57PKDxNkx8MdCwtlIN0njc14N8OHk)
Simply overwrite the existing files in your Skyrim game directory.

## Step 10 - Starting Skyrim

Almost there!

Simply open up Mod Organizer in the installation directory (`ModOrganizer.exe`), wait for it to load, select Skyrimified from the dropdown on the top right (if it isn't already on there) and click 'Launch'. This will open up Skyrim. Start a new game and once you're loaded in, wait for all the messages to stop flowing in.	This might take about a minute. When the Curators Companion asks if you want to scan now or later, select 'Scan Now'.

Afterwards, continue with the Mod Configuration Menu settings below. I've automated most of them, so they'll likely take you about 5 minutes tops :)

## MCM Settings

Press Escape and select 'Mod Configuration Menus' to start with this section.

### A Matter of Time
**Presets**
* Load user settings: Go

Feel free to customize this however you want. I prefer a small 24 hour clock in the bottom right.

### EasyWheel

* Wheel Keycode: Set to K by default, set it to whatever you prefer, just don't forget about it because it's very useful :)

### Follower Framework

**System**
* Save/Load Configuration
  * Load from File

### iWant RND Widgets

* User Load/Save
    * Load: GO!

### Keep it Clean
**Settings**
* Toggles
  * Start Keep It Clean: enabled

**EXIT THE MCM. Wait until the message 'Keep it Clean started' appears. Continue.**

### moreHUD

**Presets**
Save setting with PapyrusUtil
- Preset: Skyrimified
- Load User Settings?: Go

**EXIT THE MCM. Wait for moreHUD to activate, it's like 5 seconds. Go back to the MCMs.**

### Not So Fast MG

* Minimum Days Before Events
  * Saarthal Expedition: 2
  * Psijic Monk Visit: 5
  * Brelyna's Practice: 2
  * J'Zargo's Experiment: 2
  * Onmund's Request: 2

### Not So Fast MQ

* Minimum Days Before Events
  * First Dragon Sighting: 3
  * Note From Delphine: 6

### Realistic Needs
**Basic Needs**
* Start RN&D

* Auto-Reminder
    * Sound Notification: disabled

**Toggles**
* First Person Messages: enabled

### SkyUI
**General**

* Item List
  * Font Size: Small (whatever you prefer)
  * Category Icon Theme: Celtic, by GreatClone (whatever you prefer)
* Active Effects HUD
  * Icon Size: Small (whatever you prefer)

**Controls**

Set up your favorite groups here. I suggest binding the rest of the F1-F12 keys and changing Quick Save in the Skyrim Controls settings to something else.

### The Ultimate Dodge Mod

If you wish to change the defaults (Sneak on CTRL and Dodge on Mouse 5) follow the following steps.

If the dodge is not working correctly, set the rolling style to be something else and then set it back to Sidestep Only.

First, configure your dodge key **NOT** by going to the MCM, but go to the Controls tab in the System menu (where you enter MCM) and set your Sneak control binding to the dodge key you want to use.

Now go to the MCM page again and set your Sneak key there.

**Note: the dodge does not work in the starting cell - once you pick your start, feel free to try it out.**

### Timing is Everything

**Extra Options**
* Presets
  * Load Preset

**Select 'Load Preset' twice if you don't see anything changing, happens from time to time.
  
### VioLens

**Profile System**

* Menu Settings
  * Load: Skyrimified

### Widget Addon

**Main**

* General Settings
  * Load User Settings?: Do it! 
 
---

You're ready to start the game!	

Once outside the starting cell, you'll get a popup from Moonlight Tales - choose 'No Scaling'.	

You will also get a popup about Wintersun - choose the preferred deity you'd like to follow on your character build!

**Have fun!**

## Frequently Asked Questions

### I have a 21:9 monitor. Does this modlist work with it?

It does, but it requires a few additonal steps. Once the modlist is installed, open up Mod Organizer and look at the left pane.	

Search for '21x9' in the search bar at the bottom right of the left pane.
Uncheck the bottom two mods with **Disable me for 21x9 support** in the name (assuming you're sorting by priority). These are called the following:

- Better Dialogue Controls - Disable me for 21x9 support
- Dear Diary - Disable me for 21x9 support

### Is there a way to easily make the game more family friendly regarding nudity?

You can switch profiles in Mod Organizer (drop down menu at the top) and select `Skyrimified - SFW` instead of `Skyrimified - NSFW`.
If you want to swap back, you can do so at any time, but be aware that possible profile specific changes won't carry over.

### Can my PC run this modlist?

This modlist was designed with higher end machines in mind. Make sure to check the recommended system requirements up top.
Personally, I run this on the following rig:

* **Processor** AMD Ryzen 9 5950X
* **Graphics** Gigabyte GeForce RTX 2080 Super Gaming OC @ +60 Core Clock & +1200 Memory Clock
* **Memory** G.Skill Ripjaws V F4-3600C18D-32GVK
* **SSD:** Samsung QVO 860 1TB
* **Monitor:** Dell S2417DG (2560x1440)

I get around 50-85 FPS outdoors, and 90-150 inside of buildings and such.

### I want to add this mod. Can you help me with it?

I can't offer you support for mods you added yourself. Always check for conflicts with other mods in xEdit, and playtest them extensively.

### I get an error when launching Mod Organizer mentioning a missing VCRUNTIME140_1.DLL. How do I fix that?

Download and install the latest version (2019) of the Microsoft Visual C++ runtimes. [Click here](https://aka.ms/vs/16/release/vc_redist.x64.exe) to automatically download the latest version!

### When I launch the game it's stuck at a compiling shaders message on the top left. How do I fix that?

You're likely using another ENB preset. Make sure the VSync settings for your ENB are synchronized with the VSync settings in SSE Display Tweaks. By default those are disabled, so you'll likely want to disable VSync in your ENB preset via `enblocal.ini` in the Skyrim game directory.

### Is there a way to disable the character switching in dialogue?

There is, you can disable it by searching for `Alternate Conversation Camera` in Mod Organizer, double click it.

Go to the INI Files tab, select the ini and set `bSwitchTarget` from `1` to `0`.

### Can I run the modlist in different languages?

No, it will cause a ton of inconsistencies with the other mods, they do not all have a translation.

### I'm getting screen tearing. What's the best way to go about fixing that?

Personally, I use G-Sync so I don't use any sort of VSync. The best way to enable VSync is to search for `SSE Display Tweaks` in Mod Organizer, double click it and go to the `INI Files` tab. Click `SKSE\Plugins\SSEDisplayTweaks.ini`. Now look for the option saying `EnableVSync=false`, should be around line 45. Change `false` to `true`.

If you get an infinite loading screen from ENB compiling shaders afterwards, enable VSync in ENB aswell (adjust `enblocal.ini`) - if the two aren't synced up you may get this problem.

### Which settings can I adjust to improve graphical performance?

* In `SkyrimCustom.ini` (from Mod Organizer -> Wrench/Screwdriver Icon at the top -> INI Editor)
    * Change `iMinGrassSize` from `70` to `90`. You can further gain performance by incrementing this in steps of 10, but I'd stop at 120 - not much more to gain after that.
* In `SkyrimPrefs.ini` (from Mod Organizer -> Wrench/Screwdriver Icon at the top -> INI Editor)
    * Change `iShadowMapResolution` from `2048` to `1024`
* ENB Settings (in-game hit Shift + Enter)
  * Uncheck Ambient Occlusion, click Save Configuration on the top

If you still need more frames, you could turn off ENB.

Of course you could turn off other stuff, but that is beyond my support for your install. Don't change more if you don't know exactly what you're doing.

You will have to redo these settings when updating the modlist via Wabbajack.

### Can I re-enable pausing in menus?

You can enable it by searching for `Skyrim Souls RE - Updated` in Mod Organizer, double click it.

Select the `INI Files` tab, click on `SKSE\Plugins\SkyrimSoulsRE.ini`.

In the `[UNPAUSED_MENUS]` section, change the `true` values to `false` for the menus you want to be paused.

Save by either pressing the save icon above the editor, or by pressing Ctrl + S.

### I'm getting NVAPI_ACCESS_DENIED when trying to apply the NVIDIA settings, how do I fix this?

Navigate to `C:\ProgramData\NVIDIA Corporation\` and create a folder inside named `Drs`. If one exists, delete it first, then create a fresh folder with the same name.

If that doesn't work, here's a screenshot of the settings you'll want to apply: ![NVIDIA Profile Inspector Settings](Pictures/README/nvidiaProfile.png)

### My player character is moving a lot in the charactar creation menu! How do I stop him/her from moving so much?
You can open the console using `~` and type `player.sae idlestaticposeastart` to force a static animation. After you're done, simply jump once to exit the animation.


### It's too dark, I can't see anything!
If you are having trouble with the game being too dark, especially at night and during darker weathers, try the following.

First off, make sure your monitor is calibrated - I find there are a lot of calibration posts on [/r/monitors](https://old.reddit.com/r/monitors)

In the MCM menu, go to Vivid Weathers and Configuration. Here you can change Night Brightness and Interior Brightness to something brighter. You can also go to the CLARALUX MCM menu and scroll down and change the preset to CLARALUX DEFAULT.

Finally, if all of that remains too dark, you may want to consider changing your ENB. Rudy's ENB for Vivid Weathers is much brighter, has more vibrant fantasy feel, and supports different weather changes. Natural View Tamriel for Vivid Weathers is much brighter, more realistic and feels colder, and has more straightforward options for further tweaking; however, it does not have as much variance with the weather changes.**Keep in mind that you'll have to disable VSync in `enblocal.ini` to make sure the ENB doesn't get stuck compiling shaders!**

## Credits

**DarkLadyLexy** for creating the incredible list this modlist has originally been built off: Lexy's Legacy of the Dragonborn.	

**Mdc211** for getting me into Wabbajack in the first place, and this fork is actually based off his Wabbajack modlist. Wabbaception!

**halgari**, **erri120**, **Noggog** and others for developing Wabbajack.	

**All the other awesome people in the Wabbajack community!**
